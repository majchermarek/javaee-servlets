package com.example.servletjspdemo.service;

import java.util.HashMap;
import java.util.Map;

import com.example.servletjspdemo.domain.Instrument;

public class InstrumentStorage {
	
	public HashMap<Instrument, Integer> map = new HashMap<Instrument, Integer>();

	
	public InstrumentStorage(){
		Instrument i1 = new Instrument("gitara");
		Instrument i2 = new Instrument("trąbka");
		Instrument i3 = new Instrument("skrzypce");
		
		map.put(i1,1);
		map.put(i2,2);
		map.put(i3,3);
	}
	
	public Instrument getInstById(int id){
		for(Instrument inst : map.keySet()){
			if(inst.id == id){
				return inst;
			}
		}
		return null;
	}
	
	public void decrement(Instrument instrument){
		int newQuantity = map.get(instrument) - 1;
		map.put(instrument, newQuantity);
	}
}
