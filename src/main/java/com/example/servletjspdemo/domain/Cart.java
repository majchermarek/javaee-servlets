package com.example.servletjspdemo.domain;

import java.util.HashMap;
import java.util.Map;

public class Cart {
	public Map<Instrument,Integer> map = 
			new HashMap<Instrument,Integer>();

	public void add(Instrument inst){
		int newQuantity = 1;
		if(map.get(inst) != null){
			newQuantity = map.get(inst) + 1;
		}
		map.put(inst, newQuantity);
	}
	
	public Cart(){
//		Instrument testInst1 = new Instrument("cart test inst");
//		map.put(testInst1, 3);
	}
} 
