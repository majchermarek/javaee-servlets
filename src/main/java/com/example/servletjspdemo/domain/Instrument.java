package com.example.servletjspdemo.domain;

public class Instrument {
	public String name;
	static int lastId = 0;
	public int id;
	
	public Instrument(String new_name){
		lastId++;
		id = lastId;
		name = new_name;
	}
	
}
