package com.example.servletjspdemo.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.servletjspdemo.domain.Cart;
import com.example.servletjspdemo.domain.Instrument;
import com.example.servletjspdemo.service.InstrumentStorage;

@WebServlet(urlPatterns = "/cart-add/*")
public class CartAdd extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String[] pathInfo = req.getPathInfo().split("/");
		int id = Integer.parseInt(pathInfo[1]);
		
		InstrumentStorage storage = (InstrumentStorage) getServletContext().getAttribute("storage");
		Instrument instrumentAdded = storage.getInstById(id);
		
		Cart cart = (Cart) req.getSession().getAttribute("cart");
		if(cart == null){
			cart = new Cart();
			req.getSession().setAttribute("cart", cart);
		}
		
		cart.add(instrumentAdded);
		storage.decrement(instrumentAdded);
		res.sendRedirect("../index.jsp");
	}
}
