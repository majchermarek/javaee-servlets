package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.servletjspdemo.service.InstrumentStorage;

@WebServlet(urlPatterns = "/start")
public class StartServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		getServletContext().setAttribute("storage", new InstrumentStorage());
		req.getSession().setAttribute("cart", null);
		
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		out.println("<h1>zainicjalizowano baze w kontekscie aplikacji</h1>");
		out.println("<br><a href='index.jsp'>INDEX</a>");
		out.close();
	}
}
