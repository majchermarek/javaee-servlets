package com.example.servletjspdemo.web;

import java.awt.List;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.servletjspdemo.domain.Cart;
import com.example.servletjspdemo.domain.Instrument;
import com.example.servletjspdemo.service.InstrumentStorage;

@WebServlet(urlPatterns = "/cart-update")
public class CartUpdate extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		Map<String,String[]> paramsMap = req.getParameterMap();
		
		InstrumentStorage storage = (InstrumentStorage) getServletContext().getAttribute("storage");
		Cart cart = (Cart) req.getSession().getAttribute("cart");
		
		// check
		if(storage == null){
			response.sendRedirect("start");
		}
		if(cart == null){
			response.sendRedirect("index.jsp");
		}
		
		ArrayList<Instrument> removeInstruments = new ArrayList<Instrument>();


		
		for(Instrument inst : cart.map.keySet()){
			int storageQuantity = storage.map.get(inst);
			int cartQuantity = cart.map.get(inst);
			
			if("on".equals(req.getParameter("remove_"+inst.id))){
				// remove from cart
				removeInstruments.add(inst);
				// update storage
				storage.map.put(inst,storageQuantity+cartQuantity);
			}
			else if(req.getParameter("quantity_"+inst.id)!=null){
				int quantityNew = Integer.parseInt(req.getParameter("quantity_"+inst.id));
	
				// check if quantity positive
				if(quantityNew < 1){
					quantityNew = 1;
				}
				
				// check if enough items in storage
				int quantityRise = quantityNew - cartQuantity;
				if(quantityRise > storageQuantity){
					quantityRise = storageQuantity;
					quantityNew = cartQuantity + quantityRise;
				}
				
				// update cart & storage 
				cart.map.put(inst, quantityNew);
				storage.map.put(inst, storageQuantity-quantityRise);
			}
		}
		
		removeInstruments.stream().forEach(inst -> cart.map.remove(inst));
		
		response.sendRedirect("index.jsp");
	}
}
