<%@page import="com.example.servletjspdemo.domain.Instrument"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Demo application</title>
    </head>
    <body>
	
<jsp:useBean id="storage" class="com.example.servletjspdemo.service.InstrumentStorage" scope="application" />
<jsp:useBean id="cart" class="com.example.servletjspdemo.domain.Cart" scope="session" />
	
	
	<h2>Cart</h2>
<%

if(cart != null && cart.map.size()>0){
	%>
   	<form action="cart-update">
   	<ul>
   	
	<%
	for(Instrument inst : cart.map.keySet()){
		%>
		<li>
			<%= inst.name %> 
			Ilosc: <%= cart.map.get(inst) %>
			<input type="number" name="quantity_<%= inst.id %>" value="<%= cart.map.get(inst) %>">
			<input type="checkbox" name="remove_<%= inst.id %>">
		</li>
		<%
	}
	%>
	
	</ul>
	<input type="submit" value="aktualizuj">
	</form>
	<%
}
else {
	%> KOSZYK PUSTY <%
}
%>


	
	

        <h2>Shop</h2>
<%
//InstrumentStorage storage = (InstrumentStorage) getServletContext().getAttribute("storage");

if(storage == null){
	response.sendRedirect("start");
}

for(Map.Entry<Instrument, Integer> entry : storage.map.entrySet()){
	%>
	instrument (id: <%= entry.getKey().id %> ): <%= entry.getKey().name %> 
	(ilosc: <%= entry.getValue() %>)
	<%
	if(entry.getValue() > 0){
		%>
		<a href="cart-add/<%= entry.getKey().id %>">do koszyka</a>
		<%		
	}
	%> 
	<br>
	<% 
}
%>

<br><br>
<a href="start">Resetuj magazyn</a>
    </body>
</html>
